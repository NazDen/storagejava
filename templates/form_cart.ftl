<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Cart</title>
    <style type="text/css">
    </style>
</head>
<body>
<#if condition??>
    <p>${login} cart</p>
    <a href="/store">To store</a>
    <a href="/logout">Log Out</a>

    <#list dataForList as listItem>
        <form action="/cart" method="post">
        <span>${listItem.getName()} price: ${listItem.getPrice()}</span>
        <button value="${listItem.id()}" type="submit" name="plus">+</button>
        <button value="${listItem.id()}" type="submit" name="minus">-</button>
        <button value="${listItem.id()}" type="submit" name="delete">delete</button>
        </form>
    </#list>
    <#list carts as cart>
        <span>quantity: ${cart.getQuantity()}</span>
    </#list>
<#else>
    <p>You have to login to enter your cart</p>
    <a href="/login">Log In</a>
</#if>
</body>
</html>