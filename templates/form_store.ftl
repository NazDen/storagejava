<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Store</title>
</head>
<body>
<#if condition??>
    <p>hello ${login}</p>
    <a href="/cart">Cart</a>
    <a href="/logout">Log Out</a>
<#else>
    <a href="/login">Log In</a>
</#if>
<ul>
    <form action="/store" method="post" onsubmit="false">
    <#list dataForList as listItem>
    <li>${listItem.getName()}</li>
        <#if condition??>
                <button type="submit" name="addToCart" value="${listItem.id()}" >add to cart</button>
        <#else>
        </#if>
    </#list>
    </form>
</ul>
</body>
</html>