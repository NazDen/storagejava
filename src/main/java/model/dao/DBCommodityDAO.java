package model.dao;
import model.dto.Commodity;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

public class DBCommodityDAO implements DAO<Commodity> {
    private Connection conn;

    public DBCommodityDAO(Connection conn) {
        this.conn = conn;
    }

    @Override
    public void add(Commodity commodity) {
        try(PreparedStatement ps = conn.prepareStatement("INSERT into naz_comodities(name, price) values (?,?)")){
            ps.setString(1,commodity.getName());
            ps.setInt(2,commodity.getPrice());
            ps.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Commodity get(int id) {
        Commodity commodity = null;
        try(PreparedStatement ps = conn.prepareStatement("select * from naz_comodities where id = ?")) {
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            while (rs.next()){
                commodity = new Commodity(rs.getInt("id"), rs.getString("name"), rs.getInt("price"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return commodity;
    }

    @Override
    public Collection<Commodity> all() {
        List<Commodity> commodities= new ArrayList<>();
        try (PreparedStatement ps = conn.prepareStatement("select * from naz_comodities")){
            ResultSet rs = ps.executeQuery();
            while (rs.next()){
                Commodity commodity = new Commodity(rs.getInt(1),rs.getString(2),rs.getInt(3));
                commodities.add(commodity);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return commodities;
    }

    @Override
    public int id(Commodity commodity) {
        try(PreparedStatement ps = conn.prepareStatement("select id from naz_comodities where name = ? and price = ?")) {
            ps.setString(1,commodity.getName());
            ps.setInt(2,commodity.getPrice());
            ResultSet rs = ps.executeQuery();
            while (rs.next()){
                commodity.setId(rs.getInt("id"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return commodity.id();
    }

    @Override
    public boolean exist(Commodity commodity) {
        String name = null;
        try {
            PreparedStatement ps = conn.prepareStatement("select name from naz_comodities where name = ?");
            ps.setString(1,commodity.getName());
            ResultSet rs = ps.executeQuery();
            while (rs.next()){
                name = rs.getString(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return commodity.getName().equals(name);
    }

    @Override
    public void remove(int id) {
        try(PreparedStatement ps = conn.prepareStatement("delete from naz_comodities where id=?")){
            ps.setInt(1,id);
            ps.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
