package model.dao;
import model.dto.Identifiable;

import java.util.Collection;

public interface DAO <T extends Identifiable> {
    void add(T object);
    T get(int id);
    Collection<T> all();
    int id(T object);//>0 true
    boolean exist(T object);
    void remove(int id);
}
