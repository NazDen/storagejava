package model.dao;
import model.dto.Cart;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class DBCartDAO implements DAO<Cart> {
    private Connection conn;

    public DBCartDAO(Connection conn) {
        this.conn = conn;
    }

    @Override
    public void add(Cart cart) {
        try(PreparedStatement ps = conn.prepareStatement("insert into naz_cart(user_id, c_id, quantity) VALUES (?, ?, ?);")){
            ps.setInt(1,cart.getUserId());
            ps.setInt(2,cart.getCommodityId());
            ps.setInt(3, cart.getQuantity());
            ps.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Cart get(int id) {
        Cart cart= null;
        try(PreparedStatement ps = conn.prepareStatement("select * from naz_cart where id = ?")) {
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            while (rs.next()){
                cart = new Cart(rs.getInt("id"), rs.getInt("user_id"), rs.getInt("c_id"), rs.getInt("quantity"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return cart;
    }

    @Override
    public Collection<Cart> all() {
        List<Cart> carts= new ArrayList<>();
        try (PreparedStatement ps = conn.prepareStatement("select * from naz_cart")){
            ResultSet rs = ps.executeQuery();
            while (rs.next()){
                Cart cart = new Cart(rs.getInt("id"), rs.getInt("user_id"), rs.getInt("c_id"), rs.getInt("quantity"));
                carts.add(cart);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return carts;
    }

    @Override
    public int id(Cart cart) {
        int id = 0;
        try(PreparedStatement ps = conn.prepareStatement("select id from naz_cart where user_id = ? and c_id = ?")) {
            ps.setInt(1,cart.getUserId());
            ps.setInt(2,cart.getCommodityId());
            ResultSet rs = ps.executeQuery();
            while (rs.next()){
                id = rs.getInt("id");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return id;
    }

    @Override
    public boolean exist(Cart cart) {
        int user_id = 0;
        int c_id = 0;
        try {
            PreparedStatement ps = conn.prepareStatement("select user_id, c_id from naz_cart where user_id = ? and c_id=?");
            ps.setInt(1,cart.getUserId());
            ps.setInt(2,cart.getCommodityId());
            ResultSet rs = ps.executeQuery();
            while (rs.next()){
                user_id = rs.getInt("user_id");
                c_id = rs.getInt("c_id");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return cart.getUserId()== user_id && cart.getCommodityId() == c_id;
    }

    @Override
    public void remove(int id) {
        try(PreparedStatement ps = conn.prepareStatement("delete from naz_cart where id=?")){
            ps.setInt(1,id);
            ps.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void setQuantity(int quantity, int id) {
        try(PreparedStatement ps = conn.prepareStatement("UPDATE naz_cart SET quantity = ? WHERE id = ?")){
            ps.setInt(1,id);
            ps.setInt(2,quantity);
            ps.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void incQuantity(int id) {
        try(PreparedStatement ps = conn.prepareStatement("UPDATE naz_cart SET quantity = quantity+1 WHERE id = ?")){
            ps.setInt(1,id);
            ps.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void decQuantity(int id) {
        try(PreparedStatement ps = conn.prepareStatement("UPDATE naz_cart SET quantity = quantity-1 WHERE id = ?")){
            ps.setInt(1,id);
            ps.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public Collection<Cart> getByUser(int user_id) {
        List<Cart> carts= new ArrayList<>();
        try (PreparedStatement ps = conn.prepareStatement("select * from naz_cart WHERE user_id = ?")){
            ps.setInt(1, user_id);
            ResultSet rs = ps.executeQuery();
            while (rs.next()){
                Cart cart = new Cart(rs.getInt("id"), rs.getInt("user_id"), rs.getInt("c_id"), rs.getInt("quantity"));
                carts.add(cart);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return carts;
    }
}
