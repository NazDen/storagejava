package model.dao;
import model.dto.User;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class DBUserDAO implements DAO<User> {

    private Connection conn;

    public DBUserDAO(Connection conn) {
        this.conn = conn;
    }

    @Override
    public void add(User user) {
        try(PreparedStatement ps = conn.prepareStatement("INSERT into naz_users (login, pass) values (?,?)")){
            ps.setString(1,user.getLogin());
            ps.setString(2,user.getPassword());
            ps.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public User get(int id) {
        User user = null;
        try(PreparedStatement ps = conn.prepareStatement("select * from naz_users where id = ?")) {
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            while (rs.next()){
                user = new User(rs.getInt("id"), rs.getString("login"), rs.getString("pass"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return user;
    }

    @Override
    public Collection<User> all() {
        List<User> users= new ArrayList<>();
        try(PreparedStatement ps = conn.prepareStatement("select * from naz_users")) {
            ResultSet rs = ps.executeQuery();
            while (rs.next()){
                User user = new User(rs.getInt("id"), rs.getString("login"), rs.getString("pass"));
                users.add(user);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return users;
    }

    @Override
    public int id(User user) {
        try(PreparedStatement ps = conn.prepareStatement("select id from naz_users where login = ? and pass = ?")) {
            ps.setString(1,user.getLogin());
            ps.setString(2,user.getPassword());
            ResultSet rs = ps.executeQuery();
            while (rs.next()){
                user.setId(rs.getInt(1));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return user.id();
    }

    @Override
    public boolean exist(User user) {
        String login = null;
        try {
            PreparedStatement ps = conn.prepareStatement("select login from naz_users where login = ? ");
            ps.setString(1,user.getLogin());
            ResultSet rs = ps.executeQuery();
            while (rs.next()){
                login = rs.getString(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return user.getLogin().equals(login);
    }

    @Override
    public void remove(int id) {
        try(PreparedStatement ps = conn.prepareStatement("delete from naz_users where id=?")){
            ps.setInt(1,id);
            ps.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
