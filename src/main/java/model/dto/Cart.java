package model.dto;

public class Cart implements Identifiable {
    private int id;
    private int userId;
    private int commodityId;
    private int quantity;

    public Cart(int userId, int commodityId) {
        this.userId = userId;
        this.commodityId = commodityId;
        this.quantity = 1;
    }

    public Cart(int id, int userId, int commodityId) {
        this.id = id;
        this.userId = userId;
        this.commodityId = commodityId;
        this.quantity = 1;
    }

    public Cart(int id, int userId, int commodityId, int quantity) {
        this.id = id;
        this.userId = userId;
        this.commodityId = commodityId;
        this.quantity = quantity;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getCommodityId() {
        return commodityId;
    }

    public void setCommodityId(int commodityId) {
        this.commodityId = commodityId;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    @Override
    public int id() {
        return 0;
    }
}
