package model.dto;

public interface Identifiable {
   int id();
}
