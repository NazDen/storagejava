package servlets;
import services.UserService;
import util.FreeMarker;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Connection;
import java.util.HashMap;

public class LoginServlet extends HttpServlet {
    public final static String COOKIE_NAME = "storeUser";
    private FreeMarker template = new FreeMarker();
    private UserService userService;

    public LoginServlet(Connection conn) {
        this.userService = new UserService(conn);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Files.copy(Paths.get("templates/form_login.html"), resp.getOutputStream());
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String login = req.getParameter("login");
        String pass = req.getParameter("pass");

        HashMap<String, Object> data = new HashMap<>();
        try {
            int userId = userService.loginAndReturnId(login, pass);
            data.put("login", login);
            Cookie cookie = new Cookie(COOKIE_NAME, Integer.toString(userId));
            resp.addCookie(cookie);
            template.render("log_ok.html", data, resp);
        } catch (IllegalStateException e){
            data.put("message", e.getMessage());
            template.render("log_err.html", data, resp);
        }
    }
}
