package servlets;
import model.dto.Cart;
import model.dto.Commodity;
import services.CartService;
import services.StoreService;
import services.UserService;
import util.FreeMarker;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static servlets.LoginServlet.COOKIE_NAME;

public class CartServlet extends HttpServlet {
    private FreeMarker template = new FreeMarker();
    private CartService cartService;
    private UserService userService;
    private StoreService storeService;

    public CartServlet(Connection conn) {
        this.cartService = new CartService(conn);
        this.userService = new UserService(conn);
        this.storeService = new StoreService(conn);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HashMap<String, Object> data = new HashMap<>();
        Cookie[] cookies = req.getCookies();
        if (cookies != null && cookies.length > 0) {
            for (Cookie cookie : cookies) {
                    data.put("condition", cookie.getName().equals(COOKIE_NAME));
                    Collection<Cart> userCarts = cartService.getByUser(Integer.parseInt(cookie.getValue()));
                    List<Commodity> collect = userCarts.stream().map(cart -> storeService.getById(cart.getCommodityId())).collect(Collectors.toList());
                    data.put("login", userService.getUser(Integer.parseInt(cookie.getValue())).getLogin());
                    data.put("dataForList", collect);
                    data.put("carts", userCarts);
                    template.render("form_cart.ftl", data, resp);
                }
        }else {
            template.render("form_cart.ftl", data, resp);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Map<String, String[]> parameterMap = req.getParameterMap();
        Cookie[] cookies = req.getCookies();
        HashMap<String, Object> data = new HashMap<>();
        int user_id = 0;
        if (cookies != null && cookies.length > 0) {
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals(COOKIE_NAME)) {
                    user_id = Integer.parseInt(cookie.getValue());
                }
            }
        }
        if (parameterMap.containsKey("plus")) {
            int plus = Integer.parseInt(Stream.of(parameterMap.get("plus")).findFirst().get());
            cartService.incQuantity(user_id, plus);
            Commodity byId = storeService.getById(plus);
            System.out.println(byId);
            data.put("commodity", storeService.getById(plus));
            template.render("cart_plus.html", data, resp);
        } else if (parameterMap.containsKey("minus")) {
            int minus = Integer.parseInt(Stream.of(parameterMap.get("minus")).findFirst().get());
            cartService.decQuantity(user_id, minus);
            data.put("commodity", storeService.getById(minus));
            template.render("cart_minus.html", data, resp);
        } else if (parameterMap.containsKey("delete")) {
            int delete = Integer.parseInt(Stream.of(parameterMap.get("delete")).findFirst().get());
            cartService.delete(user_id, delete);
            data.put("commodity", storeService.getById(delete));
            template.render("cart_delete.html", data, resp);
        }
    }
}
