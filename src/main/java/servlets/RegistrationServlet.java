package servlets;
import services.UserService;
import util.FreeMarker;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Connection;
import java.util.HashMap;

public class RegistrationServlet extends HttpServlet {
    private FreeMarker template = new FreeMarker();
    private UserService userService;

    public RegistrationServlet(Connection conn) {
        this.userService = new UserService(conn);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Files.copy(Paths.get("templates/form_reg.html"), resp.getOutputStream());
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String login = req.getParameter("login");
        String pass = req.getParameter("pass");
        String pass2 = req.getParameter("pass2");
        HashMap<String, Object> data = new HashMap<>();
        data.put("login", login);
        try{
            userService.addUserInDB(login, pass, pass2);
            template.render("reg_ok.html",data,resp);
        } catch (IllegalStateException e){
            data.put("message", e.getMessage());
            template.render("reg_err.html",data,resp);
        }
    }
}
