package servlets;
import model.dto.Commodity;
import services.CartService;
import services.StoreService;
import services.UserService;
import util.FreeMarker;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;
import static servlets.LoginServlet.COOKIE_NAME;

public class StoreServlet extends HttpServlet {
    private FreeMarker template= new FreeMarker();
    private StoreService store;
    private UserService userService;
    private CartService cartService;

    public StoreServlet(Connection conn) {
        this.store = new StoreService(conn);
        this.userService = new UserService(conn);
        this.cartService = new CartService(conn);

    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Collection<Commodity> all = store.getAll();
        HashMap<String, Object> data = new HashMap<>();
        data.put("dataForList",all);
        Cookie[] cookies = req.getCookies();
        if (cookies != null && cookies.length>0) {
            for (Cookie cookie : cookies){
                    data.put("condition", cookie.getName().equals(COOKIE_NAME));
                    data.put("login", userService.getUser(Integer.parseInt(cookie.getValue())).getLogin());
                }
            }
        template.render("form_store.ftl",data, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HashMap<String, Object> data = new HashMap<>();
        Cookie[] cookies1 = req.getCookies();
        Integer user_id = Stream.of(cookies1).filter(cookie -> cookie.getName().equals(COOKIE_NAME)).findFirst().map(cookie -> Integer.parseInt(cookie.getValue())).get();
        try {
            Map<String, String[]> parameterMap = req.getParameterMap();
            String[] addToCarts = parameterMap.get("addToCart");
            Cookie[] cookies = req.getCookies();
            if (cookies != null && cookies.length > 0) {
                for (Cookie cookie : cookies) {
                    if (cookie.getName().equals(COOKIE_NAME)) {
                        Arrays.asList(addToCarts).forEach(c_id -> {
                            cartService.addItem(Integer.parseInt(cookie.getValue()), Integer.parseInt(c_id));
                            data.put("commodity", store.getById(Integer.parseInt(c_id)).getName());
                        });
                        data.put("login", userService.getUser(Integer.parseInt(cookie.getValue())).getLogin());
                        template.render("store_ok.html", data, resp);
                    }
                }
            }
        }catch (IllegalStateException e) {
            data.put("login", userService.getUser(user_id).getLogin());
            data.put("message", e.getMessage());
            template.render("store_err.html", data, resp);
        }
    }
}
