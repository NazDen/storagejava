package services;
import model.dao.DBCartDAO;
import model.dto.Cart;
import java.sql.Connection;
import java.util.Collection;

public class CartService {

    private DBCartDAO dbCartDAO;

    public CartService(Connection conn) {
        this.dbCartDAO = new DBCartDAO(conn);
    }

    public void addItem(int user_id, int c_id){
        Cart cart = new Cart(user_id, c_id);
        if (dbCartDAO.exist(cart)){
            throw new IllegalStateException("You have this commodity in your cart.");
        }else {
            dbCartDAO.add(cart);
        }
    }

    public void setQuantity(int quantity, int id){
        dbCartDAO.setQuantity(quantity, id);
    }

    public void incQuantity(int user_id, int c_id ){
        Cart cart = new Cart(user_id, c_id);
        int id = dbCartDAO.id(cart);
        dbCartDAO.incQuantity(id);
    }

    public void decQuantity(int user_id, int c_id){
        Cart cart = new Cart(user_id, c_id);
        int id = dbCartDAO.id(cart);
            if (dbCartDAO.get(id).getQuantity()<2){
                dbCartDAO.remove(id);
            }else {
                dbCartDAO.decQuantity(id);
            }
    }

    public Collection<Cart> getByUser(int user_id){
        return dbCartDAO.getByUser(user_id);
    }

    public void delete(int user_id, int c_id){
        Cart cart = new Cart(user_id, c_id);
        int id = dbCartDAO.id(cart);
        dbCartDAO.remove(id);
    }
}
