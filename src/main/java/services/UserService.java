package services;
import model.dao.DAO;
import model.dao.DBUserDAO;
import model.dto.User;
import java.sql.Connection;

public class UserService {

    private DAO<User> dbUserDAO;

    public UserService(Connection conn) {
        this.dbUserDAO = new DBUserDAO(conn);
    }

    public void addUserInDB(String login, String password, String passwordRepeat) {
        User user = new User(login, password);
        if (!password.equals(passwordRepeat)){
            throw new IllegalStateException("Password does not match.");
        }else if (login.length() == 0 && password.length() == 0) {
            throw new IllegalStateException("Parameters are missing.");
        } else if (dbUserDAO.exist(user)) {
            throw new IllegalStateException("User with such login already exists.");
        } else {
            dbUserDAO.add(user);
        }
    }

    public int loginAndReturnId(String login, String password){
        User user = new User(login, password);
        if (login.length() == 0 && password.length() == 0) {
            throw new IllegalStateException("Parameters are missing.");
        }else if (dbUserDAO.id(user)>0){
            user.setId(dbUserDAO.id(user));
        }else {
            throw new IllegalStateException("There are not user with such login. Please enter correct Data or sign up before log in.");
        }
        return user.id();
    }

    public User getUser(int id){
        return dbUserDAO.get(id);
    }


    }
