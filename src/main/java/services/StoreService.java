package services;
import model.dao.DAO;
import model.dao.DBCommodityDAO;
import model.dto.Commodity;
import java.sql.Connection;
import java.util.Collection;

public class StoreService {
    private DAO<Commodity> store;

    public StoreService(Connection conn) {
        this.store = new DBCommodityDAO(conn);
    }

    public Collection<Commodity> getAll(){
       return store.all();
    }

    public Commodity getById(int id){
        return store.get(id);
    }


}
