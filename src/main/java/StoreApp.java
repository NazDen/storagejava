import model.DBconnection;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import servlets.*;

import java.sql.Connection;

public class StoreApp {
    public static void main(String[] args) throws Exception {
        Connection conn = new DBconnection().connection();

        ServletContextHandler handler = new ServletContextHandler();
        handler.addServlet(new ServletHolder(new RegistrationServlet(conn)), "/reg");
        handler.addServlet(new ServletHolder(new LoginServlet(conn)), "/login");
        handler.addServlet(new ServletHolder(new StoreServlet(conn)), "/store");
        handler.addServlet(new ServletHolder(new CartServlet(conn)), "/cart");
        handler.addServlet(new ServletHolder(new LogoutServlet()), "/logout");

        Server server = new Server(80);
        server.setHandler(handler);
        server.start();
        server.join();


    }
}
